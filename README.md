# Frontend Blog

A simple blog app in React JS for the frontend with a laravel rest api backend.

Open a terminal window and go to the root of this project.
```sh
$ cd backend-exam
$ npm install
$ npm start
```

Open another terminal window and go to the root of this project.
Make sure you are running your local mysql database.
```sh
$ cd backend-exam/api
$ composer install
$ php artisan migrate
$ php artisan serve
```
