import React, { useEffect, useState } from "react";
import axios from "axios";
import config from "config";

import "./Logout.scss";

function Logout() {
  const [message, setMessage] = useState("");
  useEffect(() => {
    logout();
  }, []);

  const logout = () => {
    axios({
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      baseURL: config.api,
      url: "/logout",
      method: "POST"
    }).then(res => {
      localStorage.clear();
      setMessage(res.data.message);
      setTimeout(() => {
        window.location.href = "/";
      }, 2000);
    });
  };

  return (
    <div className="logout">
      <h2>{message ? message : ""}</h2>
    </div>
  );
}

export default Logout;
